import { FtExtendedTopic } from "./ft-topic";

export interface FtTopicByMap {
  title: string;
  topics: FtExtendedTopic[];
}