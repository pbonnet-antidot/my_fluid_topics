import { FtSuggestionEditorialType } from "./ft-suggestion-editorial-type";
import { FtSuggestionType } from "./ft-suggestion-type";

export interface FtSuggestion {
  type: FtSuggestionType;
  editorialType: FtSuggestionEditorialType;
  value: string;
  filenameExtension: string;
  mimeType: string;
}