import { FtMetadata } from "./ft-metadata";
import { FtTopicOpenMode } from "./ft-topic-open-mode";
import { FtTopicSource } from "./ft-topic-source";

export interface FtTopic {
  breadcrumb: string[];
  contentId: string;
  contentUrl: string;
  htmlExcerpt: string;
  htmlTitle: string;
  lastEditionDate: string;
  mapId: string;
  mapTitle: string;
  metadata: FtMetadata[];
  openMode: FtTopicOpenMode;
  readerUrl: string;
  resources: string[];
  source: FtTopicSource;
  title: string;
  tocId: string;
  topicUrl: string;
}

export interface FtExtendedTopic {
  breadcrumb: string[];
  contentId: string;
  contentUrl: string;
  htmlExcerpt: string;
  htmlTitle: string;
  lastEditionDate: string;
  mapId: string;
  mapTitle: string;
  metadata: FtMetadata[];
  openMode: FtTopicOpenMode;
  readerUrl: string;
  resources: string[];
  source: FtTopicSource;
  title: string;
  tocId: string;
  topicUrl: string;
  metadataVariableAxisValue?: string;
}