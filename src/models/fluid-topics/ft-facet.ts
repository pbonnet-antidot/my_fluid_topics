import { FtRootNode } from "./ft-root-node";

export interface FtFacet {
  key?: string;
  label?: string;
  hierarchical?: boolean;
  multiSelectionable?: boolean;
  rootNodes: FtRootNode[];
}