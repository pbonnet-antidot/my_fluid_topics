import { FtChildNode } from "./ft-child-node";

export interface FtRootNode {
  "value": string;
  "label": string;
  "selected": boolean;
  "totalResultsCount": number;
  "childNodes": FtChildNode[];
  "descendantSelected": boolean;
}