export interface FtPaging {
  currentPage?: number;
  totalResultsCount?: number;
  totalCluestersCount?: number;
  isLastPage?: boolean;
}