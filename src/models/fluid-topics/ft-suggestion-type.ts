export enum FtSuggestionType {
  MAP = 'MAP',
  DOCUMENT = 'DOCUMENT',
  TOPIC = 'TOPIC'
}