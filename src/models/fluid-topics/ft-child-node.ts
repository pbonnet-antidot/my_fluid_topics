export interface FtChildNode {
  value?: string;
  label?: string;
  selected?: boolean;
  totalResultsCount?: number;
  childNodes: FtChildNode[];
  descendantSelected?: boolean;
}