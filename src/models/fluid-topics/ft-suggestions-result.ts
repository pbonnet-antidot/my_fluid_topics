import { FtSuggestion } from "./ft-suggestion";

export interface FtSuggestionsResult {
  suggestions: FtSuggestion[];
}

export const defaultSuggestionResult: FtSuggestionsResult = {
  suggestions: []
}