import { FtFacet } from "./ft-facet";
import { FtPaging } from "./ft-paging";
import { SearchResultElement } from "./ft-search-result-element";

export interface FtSearchResult {
  facets: FtFacet[];
  paging?: FtPaging;
  results: SearchResultElement[];
}

export const defaultSearchResult: FtSearchResult = {
  facets: [],
  paging: undefined,
  results: []
}