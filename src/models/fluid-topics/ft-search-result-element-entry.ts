import { FtMap } from "./ft-map";
import { FtSearchResultElementEntryType } from "./ft-search-result-element-entry-type";
import { FtTopic } from "./ft-topic";

export interface FtSearchResultElementEntry {
  type?: FtSearchResultElementEntryType;
  topic?: FtTopic;
  map?: FtMap;
}