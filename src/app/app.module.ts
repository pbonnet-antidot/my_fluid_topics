import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzListModule } from 'ng-zorro-antd/list';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { CdkAccordionModule } from '@angular/cdk/accordion';
import { NzTagModule } from 'ng-zorro-antd/tag';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NzInputModule,
    FormsModule,
    ReactiveFormsModule,
    NzListModule,
    ScrollingModule,
    NzCollapseModule,
    CdkAccordionModule,
    NzTagModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
