import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { debounceTime, distinctUntilChanged, Observable } from 'rxjs';
import { firstValueOfControl, untilDestroyed } from 'src/helpers/rxjs-utils';
import { FtDocument } from 'src/models/fluid-topics/ft-document';
import { FtMap } from 'src/models/fluid-topics/ft-map';
import { FtSearchResult } from 'src/models/fluid-topics/ft-search-result';
import { FtSearchResultElementEntryType } from 'src/models/fluid-topics/ft-search-result-element-entry-type';
import { FtSuggestion } from 'src/models/fluid-topics/ft-suggestion';
import { FtSuggestionsResult } from 'src/models/fluid-topics/ft-suggestions-result';
import { FtExtendedTopic, FtTopic } from 'src/models/fluid-topics/ft-topic';
import { FtTopicByMap } from 'src/models/fluid-topics/ft-topic-by-map';
import { FluidTopicsService } from 'src/services/fluid-topics-api-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  title = 'my_fluid_topics';

  searchForm: FormGroup;

  documents: FtDocument[] = [];
  maps: FtMap[] = [];
  searchedTopicsByMap: FtTopicByMap[] = [];
  suggestionResult?: FtSuggestionsResult;
  suggestions: FtSuggestion[] = [];
  searchResult?: FtSearchResult;
  suggestionEnabled: boolean = true;
  currentMap?: string;

  defaultSearch = {
    name: ''
  };

  constructor(
    private fluidTopicsService: FluidTopicsService,
    private formBuilder: FormBuilder
  ) {
    this.searchForm = this.formBuilder.group(this.defaultSearch);
  }

  ngOnInit() {

    firstValueOfControl(this.searchForm).pipe(debounceTime(500), distinctUntilChanged())
    .pipe(
      untilDestroyed(this)
    )
    .subscribe(_ => {
      this.suggestionEnabled = false;
      if (this.searchForm.controls['name'].value) {
        this.fluidTopicsService.fetchSearchResult(this.searchForm.controls['name'].value);
        this.fluidTopicsService.fetchSuggest(this.searchForm.controls['name'].value);
      } else {
        this.suggestions = [];
        this.searchedTopicsByMap = [];
      }
    });

    this.fluidTopicsService.searchResult$
    .pipe(
      untilDestroyed(this),
    )
    .subscribe(searchResult => {
      this.searchResult = searchResult;
      if (searchResult) {
        this.parseSearchResult(searchResult);
      }
    });

    this.fluidTopicsService.suggestionResult$
    .pipe(
      untilDestroyed(this),
    )
    .subscribe(suggestionResult => {
      console.log('suggestionResult : ', suggestionResult);
      this.suggestionEnabled = true;
      this.suggestionResult = suggestionResult;
      if (suggestionResult && suggestionResult.suggestions && suggestionResult.suggestions.length > 0) {
        this.suggestions = [...suggestionResult.suggestions];
        if (suggestionResult.suggestions.length === 1 && suggestionResult.suggestions[0].value === this.searchForm.controls['name'].value) {
          this.suggestionEnabled = false;
        }
      } else {
        this.suggestions = [];
      }
    });

    this.fluidTopicsService.maps$
    .pipe(
      untilDestroyed(this),
    )
    .subscribe(maps => {
      this.maps = maps;
    });

    this.fluidTopicsService.documents$
    .pipe(
      untilDestroyed(this),
    )
    .subscribe(documents => {
      this.documents = documents;
    });

    this.fluidTopicsService.fetchDocuments();
    this.fluidTopicsService.fetchMaps();
  }

  ngOnDestroy() { }

  redirectToDocument(id: any) {
    this.fluidTopicsService.getDocumentContent(id).subscribe(content => {
      let w = window.open('about:blank');
      if (w !== null) {
        w.document.open();
        w.document.write(content);
        w.document.close();
      }
    })
  }

  getTopicContent(mapId: string, contentId: string) {
    this.fluidTopicsService.getTopicContent(mapId, contentId).subscribe(content => {
      let w = window.open('about:blank');
      if (w !== null) {
        w.document.open();
        w.document.write(content);
        w.document.close();
      }
    });
  }

  parseSearchResult(searchResult: FtSearchResult) {
    const results = searchResult.results;
    let topicsByMap: FtTopicByMap[] = [];
    for (const result of results) {
      const entries = result.entries;
      const metadataVariableAxis = result.metadataVariableAxis;
      for (const entry of entries) {
        switch (entry.type) {
          case FtSearchResultElementEntryType.MAP:
            break;
          case FtSearchResultElementEntryType.TOPIC:
            let topic = entry.topic;
            if (topic) {
              let found = topicsByMap.find(e => e.title === topic?.title);
              if (found) {
                let correspondingMetadata = topic.metadata.find(e => e.key === result.metadataVariableAxis);
                let extendedTopic: FtExtendedTopic = { ...topic };
                if (correspondingMetadata && correspondingMetadata.values && correspondingMetadata.values.length > 0) {
                  extendedTopic.metadataVariableAxisValue = correspondingMetadata.values[0];
                }
                found.topics = [...found.topics, extendedTopic ];
              } else {
                let newTopicsByMap: FtTopicByMap = {
                  title: topic.title,
                  topics: [topic]
                };
                topicsByMap.push(newTopicsByMap);
              }
            }
            break;
          default:
            break;
        }
      }
    }
    this.searchedTopicsByMap = topicsByMap;
  }

  setQuery(selectedQuery: string) {
    this.searchForm.controls['name'].setValue(selectedQuery);
  }

  disableSuggestions() {
    if (this.suggestionEnabled) {
      this.suggestionEnabled = false;
    }
  }

}
