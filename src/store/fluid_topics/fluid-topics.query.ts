import { HttpClient, HttpHeaders, HttpParams, HttpParamsOptions } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Query } from "@datorama/akita";
import { Observable } from "rxjs";
import { FtDocument } from "src/models/fluid-topics/ft-document";
import { FtMap } from "src/models/fluid-topics/ft-map";
import { FtSearchResult } from "src/models/fluid-topics/ft-search-result";
import { FtSuggestionsResult } from "src/models/fluid-topics/ft-suggestions-result";
import { FluidTopicsState } from "./fluid-topics.state";
import { FluidTopicsStore } from "./fluid-topics.store";

@Injectable({ providedIn: 'root' })
export class FluidTopicsQuery extends Query<FluidTopicsState> {

  private readonly baseUrl = '/api/khub';

  documents$ = this.select('documents');
  maps$ = this.select('maps');
  searchResult$ = this.select('searchResult');
  suggestionResult$ = this.select('suggestionResult');

  httpOptions = {
    headers: new HttpHeaders({ 
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json'
    })
  };

  constructor(
    protected http: HttpClient,
    protected override store: FluidTopicsStore,
  ) { 
    super(store);
  }

  fetchDocuments() {
    this.getDocuments()
      .subscribe(documents => this.store.update(_ => ({ documents })));
  }

  private getDocuments() {
    return this.http.get<FtDocument[]>(this.baseUrl + '/documents', this.httpOptions);
  }

  fetchMaps() {
    this.getMaps()
      .subscribe(maps => this.store.update(_ => ({ maps })));
  }

  private getMaps() {
    return this.http.get<FtMap[]>(this.baseUrl + '/maps', this.httpOptions);
  }

  fetchSearchResult(query: string) {
    this.searchQuery(query)
      .subscribe(searchResult => this.store.update(_ => ({ searchResult })));
  }

  private searchQuery(query: string): Observable<FtSearchResult> {
    let params = new HttpParams({});
    params.append('query', query);
    let options = {
      headers: new HttpHeaders({ 
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
      }),
    };
    return this.http.post<FtSearchResult>(this.baseUrl + '/clustered-search', { "query": query }, options);
  }

  getDocumentContent(id: string) {

    let options = {
      headers: new HttpHeaders({ 
        'Access-Control-Allow-Origin': '*',
      }),
      responseType: 'text' as const
    };

    return this.http.get(this.baseUrl + '/documents/' + id + '/content', options);
  }

  fetchSuggest(query: string) {
    this.suggest(query)
      .subscribe(suggestionResult => this.store.update(_ => ({ suggestionResult })));
  }

  private suggest(query: string): Observable<FtSuggestionsResult> {
    let params = new HttpParams({});
    params.append('query', query);
    let options = {
      headers: new HttpHeaders({ 
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
      }),
    };
    return this.http.post<FtSuggestionsResult>(this.baseUrl + '/suggest', { "input": query }, options);
  }

  getTopicContent(mapId: string, contentId: string) {

    let options = {
      headers: new HttpHeaders({ 
        'Access-Control-Allow-Origin': '*',
      }),
      responseType: 'text' as const
    };

    return this.http.get(this.baseUrl + '/maps/' + mapId + '/topics/' + contentId + '/content', options);
  }

}