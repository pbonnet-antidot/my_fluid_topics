import { FtDocument } from "src/models/fluid-topics/ft-document";
import { FtMap } from "src/models/fluid-topics/ft-map";
import { defaultSearchResult, FtSearchResult } from "src/models/fluid-topics/ft-search-result";
import { defaultSuggestionResult, FtSuggestionsResult } from "src/models/fluid-topics/ft-suggestions-result";


export interface FluidTopicsState {
  documents: FtDocument[];
  maps: FtMap[];
  searchResult: FtSearchResult;
  suggestionResult: FtSuggestionsResult;
}

export function createInitialFluidTopicsState(): FluidTopicsState {
  return {
    documents: [],
    maps: [],
    searchResult: defaultSearchResult,
    suggestionResult: defaultSuggestionResult
  };
}