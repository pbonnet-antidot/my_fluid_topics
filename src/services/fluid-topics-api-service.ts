import { Injectable } from "@angular/core";
import { FluidTopicsQuery } from "src/store/fluid_topics/fluid-topics.query";

@Injectable({ providedIn: 'root' })
export class FluidTopicsService {

  documents$ = this.fluidTopicsQuery.documents$;
  maps$ = this.fluidTopicsQuery.maps$;
  searchResult$ = this.fluidTopicsQuery.searchResult$;
  suggestionResult$ = this.fluidTopicsQuery.suggestionResult$;

  constructor(
    private fluidTopicsQuery: FluidTopicsQuery
  ) { }

  fetchDocuments() {
    this.fluidTopicsQuery.fetchDocuments()
  }

  fetchMaps() {
    this.fluidTopicsQuery.fetchMaps();
  }

  fetchSearchResult(query: string) {
    this.fluidTopicsQuery.fetchSearchResult(query);
  }

  getDocumentContent(id: string) {
    return this.fluidTopicsQuery.getDocumentContent(id);
  }

  fetchSuggest(query: string) {
    this.fluidTopicsQuery.fetchSuggest(query);
  }

  getTopicContent(mapId: string, contentId: string) {
    return this.fluidTopicsQuery.getTopicContent(mapId, contentId);
  }
}